<!DOCTYPE html>
<html>
<head>
    <title>Résultat du binomage</title>
</head>
<body>
    <p>Bonjour {{ $binomeUtilisateurConnecte->utilisateurA->nom }},</p>
    <p>Votre binôme est : </p>
    <p>Nom : {{  $binomeUtilisateurConnecte->utilisateurB->nom }}</p>
    <p>Prénoms : {{ $binomeUtilisateurConnecte->utilisateurB->prenoms }}</p>
    <p>Sexe : {{ $binomeUtilisateurConnecte->utilisateurB->sexe }}</p>
    @php
        $dateNaissance = $binomeUtilisateurConnecte->utilisateurB->date_naissance;
        if (!empty($dateNaissance)) {
            $dateNaissance = new DateTime($dateNaissance);
            $aujourdHui = new DateTime();
            $difference = $aujourdHui->diff($dateNaissance);
            $age = $difference->y;
        }
    @endphp
    @if($binomeUtilisateurConnecte->utilisateurB->is_visible == 1)
        <li>Age : {{ $age }} ans</li>
    @endif
    <p>Merci d'utiliser notre service de tirage automatique d'un binôme. <br> Bonne collaboration avec votre binôme et bonne fête de fin d'année !</p>
</body>
</html>