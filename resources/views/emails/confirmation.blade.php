<!DOCTYPE html>
<html>
<head>
    <title>Confirmation d'inscription</title>
</head>
<body>
    <p>Bonjour {{ $user->nom }},</p>
    <p>Votre inscription à la plateforme JeunesseMeistad a été effectuée avec succès.</p>
    <p>Vos paramètres de connexion :</p>
    <p>Email : {{ $user->email }}</p>
    <p>Mot de passe : {{ $user->password }}</p>
    <p>Cliquez sur le lien ci-dessous pour vous connecter :</p>
    <a href="{{ $confirmationLink }}">Confirmer votre inscription</a>
    <p>Merci d'utiliser notre application!</p>
</body>
</html>