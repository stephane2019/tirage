<!DOCTYPE html>
<html>
<head>
    <title>Réinitialisation du mot de passe</title>
</head>
<body>
    <p>Bonjour {{ $user->nom }},</p>
    <p>Votre mot de passe a été réinitialiser avec succès.</p>
    <p>Mot de passe : {{ $password }}</p>
    <p>Cliquez sur le lien ci-dessous pour vous connecter :</p>
    <a href="{{ $confirmationLink }}">Connecter vous</a>
    <p>Merci d'utiliser notre application!</p>
</body>
</html>