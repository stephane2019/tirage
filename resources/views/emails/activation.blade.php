<!DOCTYPE html>
<html>
<head>
    <title>Confirmez votre inscription</title>
</head>
<body>
    <h1>Bonjour {{ $user->nom }} {{ $user->prenoms }},</h1>
    <p>Merci pour votre inscription ! Veuillez cliquer sur le lien ci-dessous pour activer votre compte :</p>
    <p><a href="{{ $activationLink }}">Activer mon compte</a></p>
    <p>Merci !</p>
</body>
</html>
