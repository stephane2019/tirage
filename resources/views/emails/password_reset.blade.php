<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Réinitialisation de votre mot de passe</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 20px;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            background: white;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
            padding: 20px;
        }
        h1 {
            color: #333;
        }
        p {
            line-height: 1.6;
            color: #555;
        }
        .button {
            display: inline-block;
            padding: 10px 20px;
            margin: 20px 0;
            background-color: #007bff;
            color: white;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s;
        }
        .button:hover {
            background-color: #0056b3;
        }
        .footer {
            margin-top: 20px;
            font-size: 12px;
            color: #777;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Bonjour!</h1>
        <p>Vous recevez cet e-mail parce que nous avons reçu une demande de réinitialisation de mot de passe pour votre compte.</p>
        <p>
            <a href="{{ $url }}" class="button">Réinitialiser le mot de passe</a>
        </p>
        <p>Ce lien de réinitialisation de mot de passe expirera dans 60 minutes.</p>
        <p>Si vous n'avez pas demandé la réinitialisation du mot de passe, aucune autre action n'est requise.</p>
        <p>Cordialement,<br>Jeunesse Meistad</p>
        <p class="footer">
            Si vous rencontrez des problèmes pour cliquer sur le bouton "Réinitialiser le mot de passe", copiez et collez l'URL ci-dessous dans votre navigateur :<br>
            <a href="{{ $url }}">{{ $url }}</a>
        </p>
    </div>
</body>
</html>
