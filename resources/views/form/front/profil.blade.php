@extends('form.front.layouts.layout')
@section('content')
<div class="">
    @include('form.pages.alert')
</div>
<div class="wrapper col-md-6 col-sm-11" style="margin-top: 70px;">
    <form class="form-right" method="post" action="{{ route('profil.update', $user->id) }}">
        @csrf
        @method('PUT')
        <h2 class="text-uppercase text-center">Mettre à jour mes informations</h2>
        <div class="row">
            <div class="col-sm-12 mb-3">
                <label for="nom">Nom</label>
                <input type="text" name="nom" id="nom" class="form-control input-field" required value="{{ $user->nom }}">
            </div>
            <div class="col-sm-12 mb-3">
                <label for="prenoms">Prenoms</label>
                <input type="text" name="prenoms" id="prenoms" class="form-control input-field" required value="{{ $user->prenoms }}">
            </div>
        </div>
        <div class="col-sm-12 mb-3">
            <label for="email">Email</label>
            <input type="email" class="form-control input-field" name="email" id="email" required value="{{ $user->email }}">
        </div>
        <div class="mb-3">
            <label for="sexe">Sexe</label>
            <select class="form-control input-field" id="sexe" name="sexe" required>
                <option value="homme" {{ $user->sexe == 'homme' ? 'selected' : '' }}>Homme</option>
                <option value="femme" {{ $user->sexe == 'femme' ? 'selected' : '' }}>Femme</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="date_naissance">Date de naissance</label>
            <input type="date" class="form-control input-field" name="date_naissance" id="date_naissance" max="<?php echo date('Y-m-d'); ?>" value="{{ $user->date_naissance }}">
            <span id="ageError" style="color: red;"></span>
        </div>
        @php
            if (!empty($user->date_naissance)) {
                $dateNaissance = new DateTime($user->date_naissance);
                $aujourdHui = new DateTime();
                $difference = $aujourdHui->diff($dateNaissance);
                $age = $difference->y;
            } else {
                $age = '';
            }
        @endphp
        <div class="mb-3" id="visibleAgeCheckbox" @if (empty($age)) style="display: none;" @endif>
            <label for="age">Âge</label>
            <input type="text" class="form-control" name="age" id="age" value="{{ $age }}" readonly>
        </div>
        <div class="mb-3" id="visibleCheckbox">
            <label class="option">
                J'accepte de rendre visible mon âge
                <input type="checkbox" name="visible_age" {{ $user->is_visible == 1 ? 'checked' : '' }}>
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="form-field">
            <input type="submit" value="Mise à jour de mes informations personnelles" class="col-sm-12 register" name="inscription">
        </div>
    </form>
</div>
<script src="{{ asset('/assets/profil.js') }}"></script>
@endsection