@extends('form.front.layouts.layout')

@section('content')
<div class="col-md-6 col-sm-10 m-auto mt-5" style="background-color: #f9f9f9; border-radius: 10px; padding: 20px; box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);">
    <!-- Section Bienvenue -->
    <div class="welcome-section text-center" style="background-color: #f3f9f8; padding: 30px; border-radius: 10px; box-shadow: 0 4px 12px rgba(0, 0, 0, 0.1);">
        <h1 class="text-uppercase" style="color: #b30000;">Bienvenue, {{ Auth::user()->prenoms .' '.Auth::user()->nom }}!</h1>
        <p style="font-size: 18px; color: #555;">
            Vous êtes prêt(e) pour le Secret Santa 2024 ! Nous vous souhaitons une expérience magique et pleine de surprises.
        </p>
    </div>

    <!-- Règles et Consignes -->
    <div class="rules-section" style="margin-top: 40px; background-color: #fafafa; padding: 20px; border-radius: 10px; box-shadow: 0 4px 12px rgba(0, 0, 0, 0.1);">
        <h3 style="color: #b30000;">Règles et Consignes</h3>
        <ul style="list-style: none; padding: 0;">
            <li style="padding: 10px; color: #555;">
                🎄 Gardez votre tirage secret pour surprendre votre Secret Santa !
            </li>
            <li style="padding: 10px; color: #555;">
                🎁 Préparez un cadeau avec soin et bienveillance.
            </li>
            <li style="padding: 10px; color: #555;">
                🧑‍🎄 Si vous avez des questions, contactez un administrateur.
            </li>
        </ul>
    </div>
</div>
@endsection
