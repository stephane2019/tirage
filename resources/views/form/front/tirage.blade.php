@extends('form.front.layouts.layout')
@section('content')
<div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
    <div class="text-center">
        <a href="javascript:void(0);" id="findBinomeButton" class="btn bg-primary text-white">
            <i class="fa fa-search"></i> Trouver un Secret Santa
        </a>
        <div id="loadingText" style="display: none; color: #007bff; margin-top: 10px;">
            Chargement du tirage... Merci de patienter.
        </div>
        <div id="progressBar" style="display: none; margin-top: 10px; width: 100%;">
            <div class="progress">
                <div id="progressBarInner" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
    $("#findBinomeButton").click(function() {
        // Masquer le bouton "Trouver un Secret Santa"
        $(this).hide();

        // Afficher le texte de chargement et la barre de progression
        $("#loadingText").show();
        $("#progressBar").show();

        // Simuler une progression de la barre (30 secondes au total)
        var width = 0;
        var interval = setInterval(function() {
            width++;
            $("#progressBarInner").css("width", width + "%");
            $("#progressBarInner").attr("aria-valuenow", width);

            if (width >= 100) {
                clearInterval(interval);
                // Rediriger vers la page de résultat après la progression complète
                window.location.href = "{{ route('tirage.attribuer') }}";
            }
        }, 300); // 300ms pour atteindre 100% en 30 secondes
    });
});
</script>
@endsection
