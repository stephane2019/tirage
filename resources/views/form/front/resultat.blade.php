@extends('form.front.layouts.layout')
@section('content')
<div class="container" style="margin-top: 70px;">
    <div class="row">
        @include('form.pages.alert')
    </div>
    <div style="margin-top:10px"></div>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <h3>Résultat du tirage de Secret Santa</h3>
                </div>
                <div class="card-body">
                    @if (!empty($binomeUtilisateurConnecte))
                        <p>Votre Secret Santa est :</p>
                        <div class="row">
                            <div class="col-sm-8">
                                <ul>
                                    <li>Nom : {{  $binomeUtilisateurConnecte->utilisateurB->nom }}</li>
                                    <li>Prénoms : {{ $binomeUtilisateurConnecte->utilisateurB->prenoms }}</li>
                                    <li>Sexe : {{ $binomeUtilisateurConnecte->utilisateurB->sexe }}</li>
                                    <li>Email : {{ $binomeUtilisateurConnecte->utilisateurB->email }}</li>
                                    @php
                                        $dateNaissance = $binomeUtilisateurConnecte->utilisateurB->date_naissance;
                                        if (!empty($dateNaissance)) {
                                            $dateNaissance = new DateTime($dateNaissance);
                                            $aujourdHui = new DateTime();
                                            $difference = $aujourdHui->diff($dateNaissance);
                                            $age = $difference->y;
                                        }
                                    @endphp
                                    @if($binomeUtilisateurConnecte->utilisateurB->is_visible == 1)
                                        <li>Age : {{ $age }} ans</li>
                                    @endif
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <div class="rounded-icon">
                                    @if ($binomeUtilisateurConnecte->utilisateurB->sexe == 'homme')
                                        <i class="fas fa-male"></i>
                                    @else
                                        <i class="fas fa-female"></i>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <p>Merci d'utiliser notre service de tirage automatique de Secret Santa. <br> Bonne collaboration avec votre Secret Santa et bonnes fêtes de fin d'année !</p>
                    @else
                        <p>Vous n'avez pas de Secret Santa pour le moment. <br> Cliquez ici pour trouver un Secret Santa.</p>
                        <button id="tirage-btn" class="btn btn-primary">Tirage</button>
                        <p id="loading-msg" style="display: none; color: #007bff;">Chargement du tirage... Merci de patienter.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('/assets/tirage.js') }}"></script>
@endsection
