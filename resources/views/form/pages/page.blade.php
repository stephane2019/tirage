<div class="form-left">
    <h2 class="text-uppercase">information</h2>
    <p>
        Cette application est destinée à vous attribuer un binôme de façon aléatoire.
    </p>
    <p class="text">
        <span>MEISTAD:</span>
        Une initiative des jeunes de la meistad pour mieux gérer la fête de fin d'année.
    </p>
    <p class="text">
        <span>CONSIGNE:</span>
        Pour une meileure gestion, si vous n'avez de compte vous devez contacter l'administrateur pour vous en créer un afin d'en bénéficier des fonctionnalités de l'application.
    </p>
</div>