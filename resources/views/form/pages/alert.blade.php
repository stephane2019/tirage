@if(session()->has('Ok'))
    <div class="col-sm-6 alert alert-success alert-dismissible fade show" role="alert" style="margin: 0 auto;margin-top: 70px;margin-bottom: 0px;">
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Fermer"></button>
        {{ session('Ok') }}
    </div>
@elseif(session()->has('No'))
    <div class="col-sm-6 alert alert-danger alert-dismissible fade show" style="margin: 0 auto;margin-bottom: 0px;" role="alert">
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Fermer"></button>
        {{ session('No') }}
    </div>
@elseif(session()->has('Info'))
    <div class="col-sm-6 alert alert-info alert-dismissible fade show" style="margin: 0 auto;margin-top: 70px;margin-bottom: 0px;" role="alert">
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Fermer"></button>
        {{ session('Info') }}.
    </div>
@endif
@if (session('status'))
    <div class="col-sm-6 alert alert-info alert-dismissible fade show" style="margin: 0 auto;margin-top: 70px;margin-bottom: 0px;" role="alert">
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Fermer"></button>
        {{ session('status') }}
    </div>
@endif
