@extends('form.home')
@section('content')
<div class="col-sm-9" style="padding: 20px 40px;
    position: relative;margin: auto;
    margin-bottom: -50px;">
    @include('form.pages.alert')
</div>
<div class="wrapper col-sm-10 col-md-4" style="margin-top: 70px;">
    <form method="POST" action="{{ route('admin.login') }}" class="form-right">
        @csrf
        <h2 class="text-uppercase text-center">Administration</h2>
        <div class="mb-3">
            <label>Email</label>
            <input type="email" class="form-control input-field" name="email" required>
        </div>
        <div class="row">
            <div class="col-sm-12 mb-3">
                <label for="pwd">Password</label>
                <input type="password" name="password" id="pwd" class="form-control input-field" required>
            </div>
        </div>
        <div class="form-field">
            <input type="submit" value="Connexion" class="register float-right" name="connexion">
        </div>
    </form>
</div>
@endsection
<style>
    .content {
        margin-left: 0px !important;
    }
</style>