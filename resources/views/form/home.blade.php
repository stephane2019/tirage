<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Espace administration</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.5.0/css/responsive.bootstrap5.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet">
    <link href="{{ asset('/assets/style.css') }}" rel="stylesheet">
</head>
<style>
    .content {
        margin-left: 250px;
        padding: 20px;
        flex-grow: 1;
    }
</style>
<body class="antialiased">
    @if (Auth::guard('admin')->check())
    <div class="sidebar">
        <h4 class="text-center">Jeunesse Meistad</h4>
        <a href="{{ route('admin.home') }}" class="{{ Route::currentRouteName() == 'admin.home' ? 'active' : '' }}">
            <i class="fas fa-home icon-margin"></i> Accueil
        </a>
        <a href="#" id="toggle-list">
            <i class="fas fa-list icon-margin"></i> Liste
        </a>
        <div class="submenu" id="list-submenu">
            <a href="{{ route('liste') }}" class="{{ Route::currentRouteName() == 'liste' ? 'active' : '' }}">
                <i class="fas fa-users icon-margin"></i> Utilisateurs
            </a>
            <a href="{{ route('secret_santa') }}" class="{{ Route::currentRouteName() == 'secret_santa' ? 'active' : '' }}">
                <i class="fas fa-users icon-margin"></i> Secret Santa
            </a>
        </div>
        <a href="{{ route('admin.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt icon-margin"></i> Déconnexion</a>
        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
    @endif
    <div class="content">
        @yield('content')
        @if (Auth::guard('admin')->check() && Route::currentRouteName() == 'admin.home')
            <div class="alert alert-success" style="margin-top:5em">
                Nous vous souhaitons la bienvenue, sur votre espace administrateur.
            </div>
        @endif
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.5.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.5.0/js/responsive.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        // Vérifiez l'état du sous-menu lors du chargement de la page
        if (localStorage.getItem('submenuVisible') === 'true') {
            $('#list-submenu').show(); // Afficher le sous-menu si l'état est "true"
        } else {
            $('#list-submenu').hide(); // Masquer le sous-menu par défaut
        }

        $('#myDataTable').DataTable({
            responsive: true,
            language: {
                search: "Recherche",
                show: "Afficher"
            }
        });

        // Gérer le clic sur "Liste" pour afficher ou masquer le sous-menu
        $('#toggle-list').on('click', function(e) {
            e.preventDefault(); // Empêcher le comportement par défaut du lien
            $('#list-submenu').slideToggle(); // Afficher ou masquer le sous-menu
            // Enregistrer l'état du sous-menu dans localStorage
            localStorage.setItem('submenuVisible', $('#list-submenu').is(':visible'));
        });

        // Ne pas masquer le sous-menu lorsque l'on clique sur un élément du sous-menu
        $('#list-submenu a').on('click', function(e) {
            e.stopPropagation(); // Empêcher la propagation de l'événement vers le parent
        });
    });
</script>
</html>
