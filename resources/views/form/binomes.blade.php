@extends('form.home')
@section('content')
@include('form.pages.alert')
<div class="wrapper" style="max-width: 100%;">
    <table id="myDataTable" style="width: 100%;" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Utilisateur A</th>
                <th>Utilisateur B</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($binomes as $user)
            <tr>
                <td>{{$user->utilisateurA->nom}} {{$user->utilisateurA->prenoms}}</td>
                <td>{{$user->utilisateurB->nom}} {{$user->utilisateurB->prenoms}}</td>
                <td>
                    <a href="{{ route('user.secret', $user->utilisateurA->id) }}" title="Modifier"><i class="fa fa-edit"></i></a>
                    <!-- <a href="{{ route('binomes.delete', ['id' => $user->id]) }}" title="Supprimer"><i class="fa fa-trash"></i></a> -->
                    <form action="{{ route('binomes.delete', $user->id) }}" method="POST" style="display:inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="fa fa-trash" onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce binôme ?');"></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
