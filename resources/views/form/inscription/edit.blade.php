@extends('form.home')
@section('content')
@include('form.pages.alert')
<div class="wrapper col-sm-8" style="margin-top: 70px;">
    <form class="form-right" method="post" action="{{ route('user.update', $user->id) }}">
        @csrf
        @method('PUT')
        <h2 class="text-uppercase text-center">Formulaire d'inscription</h2>
        <div class="row">
            <div class="col-sm-12 mb-3">
                <label for="nom">Nom</label>
                <input type="text" name="nom" id="nom" class="form-control input-field" required value="{{ $user->nom }}">
            </div>
        </div>
        <div class="col-sm-12 mb-3">
            <label for="prenoms">Prenoms</label>
            <input type="text" name="prenoms" id="prenoms" class="form-control input-field" required value="{{ $user->prenoms }}">
        </div>

        <div class="mb-3">
            <label for="email">Email</label>
            <input type="email" class="form-control input-field" name="email" id="email" required value="{{ $user->email }}">
        </div>
        <div class="mb-3">
            <label for="sexe">Sexe</label>
            <select class="form-control input-field" id="sexe" name="sexe" required>
                <option value="homme" {{ $user->sexe == 'homme' ? 'selected' : '' }}>Homme</option>
                <option value="femme" {{ $user->sexe == 'femme' ? 'selected' : '' }}>Femme</option>
            </select>
        </div>

        <div class="mb-3 d-flex align-items-center">
            <label for="isParticipe" class="me-2">Participe t-il au secret santa cette année ?</label>
            <input type="checkbox" name="is_participed" id="isParticipe" style="margin-top: -10px;" value="1" {{ $user->is_participed ? 'checked' : '' }}>
        </div>


        <!-- <div class="mb-3">
            <label for="isbonneA">Binone A</label>
            <input type="checkbox" name="is_binomeA" id="isbonneA" class="input-field" 
                value="1" {{ $user->is_binomeA ? 'checked' : '' }}>
        </div>

        <div class="mb-3">
            <label for="isbonneB">Binone B</label>
            <input type="checkbox" name="is_binomeB" id="is_binomeB" class="input-field" 
                value="1" {{ $user->is_binomeB ? 'checked' : '' }}>
        </div> -->

        <div class="form-field">
            <input type="submit" value="Mise à jour" class="col-sm-12 register" name="inscription">
        </div>
    </form>
</div>
@endsection