@extends('form.home')
@section('content')
@include('form.pages.alert')
<div class="wrapper col-sm-6" style="margin-top: 70px;">
    <form class="form-right" method="post" action="{{ route('user.store') }}">
        @csrf
        <h2 class="text-uppercase text-center">Formulaire d'inscription</h2>
        <div class="row">
            <div class="col-sm-12 mb-3">
                <label for="nom">Nom</label>
                <input type="text" name="nom" id="nom" class="form-control input-field" required value="{{ old('nom') }}">
            </div>
        </div>
        <div class="col-sm-12 mb-3">
            <label for="prenoms">Prenoms</label>
            <input type="text" name="prenoms" id="prenoms" class="form-control input-field" required value="{{ old('prenoms') }}">
        </div>

        <div class="mb-3">
            <label for="email">Email</label>
            <input type="email" class="form-control input-field" name="email" id="email" required value="{{ old('email') }}">
        </div>
        <div class="mb-3">
            <label for="sexe">Sexe</label>
            <select class="form-control input-field" id="sexe" name="sexe" required>
                <option value="">Selectionner le sexe</option>
                <option value="homme" {{ old('sexe') == 'homme' ? 'selected' : '' }}>Homme</option>
                <option value="femme" {{ old('sexe') == 'femme' ? 'selected' : '' }}>Femme</option>
            </select>
        </div>
        <div class="form-field">
            <input type="submit" value="Enregistrer" class="col-sm-12 register" name="inscription">
        </div>
    </form>
</div>
@endsection