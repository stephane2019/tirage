@extends('form.home')
@section('content')
@include('form.pages.alert')
<div class="d-flex justify-content-end mb-3">
    <a href="{{ route('inscription') }}" class="btn btn-primary bg-primary" ng-click="openModal()" type="button">
        <i class="fa fa-plus"></i> Nouveau
    </a>
</div>
<div class="wrapper" style="max-width: 100%;">
    <table id="myDataTable" style="width: 100%;" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Nom & Prénoms</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{$user->nom}} {{$user->prenoms}}</td>
                <td>{{$user->email}}</td>
                <td>
                <a href="{{ route('user.edit', $user->id) }}" title="Modifier"><i class="fa fa-edit"></i></a>
                @if($user->is_binomeA == 0 && $user->is_binomeB == 0) 
                    <a href="{{ route('user.delete', ['id' => $user->id]) }}" title="Supprimer"><i class="fa fa-trash"></i></a>
                @endif
                <a href="{{ route('user.reinitialiser', ['id' => $user->id]) }}" title="Reinitialiser le mot de passe"><i class="fa fa-lock"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
