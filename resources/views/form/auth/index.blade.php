@extends('form.front.layouts.layout')
@section('content')
<div class="col-sm-9" style="padding: 20px 40px; position: relative; margin: auto; margin-bottom: -50px;">
    @include('form.pages.alert')
</div>
<div class="wrapper col-sm-4" style="margin-top: 70px;">
    <form class="form-right" method="POST" action="{{ route('user.login') }}">
        @csrf
        <h2 class="text-uppercase text-center">Authentification</h2>
        <div class="mb-3">
            <label for="email">Email</label>
            <input type="email" id="email" class="form-control input-field" name="email" required>
        </div>
        <div class="row">
            <div class="col-sm-12 mb-3">
                <label for="pwd">Mot de passe</label>
                <div class="input-group">
                    <input type="password" name="password" id="pwd" class="form-control input-field" required>
                    <span class="input-group-text passwordToggle" id="togglePassword" style="cursor: pointer;">
                        <i class="fa fa-eye"></i>
                    </span>
                </div>
            </div>
            <div class="col-sm-12 mb-3">
                <a href="{{ route('password.request') }}" class="text-secondary">Mot de passe oublié ?</a>
            </div>
        </div>
        <div class="form-field">
            <input type="submit" value="Connexion" class="register float-right" name="connexion">
        </div>
        <div class="mt-3">
            <a href="{{ route('register') }}" class="text-primary">Créer un nouveau compte</a>
        </div>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="{{ asset('/assets/password.js') }}"></script>
@endsection
