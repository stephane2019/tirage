@extends('form.front.layouts.layout')
@section('content')
<div class="wrapper col-sm-4" style="margin-top: 70px;">
    <form class="form-right" method="POST" action="{{ route('verification') }}">
        @csrf
        @method('PUT')
        <h2 class="text-uppercase">Definir un nouveau mot de passe</h2>
        <div class="row">
            <div class="col-sm-12 mb-3">
                <label for="pwd">Password</label>
                <input type="password" name="password" id="pwd" class="form-control input-field">
            </div>
            <div class="col-sm-12 mb-3">
                <label for="Conf_pwd">Confirmation Password</label>
                <input type="password" name="Conf_pwd" id="Conf_pwd" class="form-control input-field">
            </div>
        </div>
        <div class="form-field">
            <input type="submit" value="Valider" class="register" name="valider">
        </div>
    </form>
</div>
@endsection