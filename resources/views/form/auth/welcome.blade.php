@extends('form.front.layouts.layout')

@section('content')
<div class="wrapper col-sm-10 col-md-6 col-lg-6" style="background-color: #f9f9f9; border-radius: 10px; padding: 20px; box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);">
    <div class="form-right">
        <!-- <img src="{{ asset('images/header.jpg') }}" alt="Header Image" class="img-fluid" style="border-radius: 10px; margin-bottom: 20px;"> -->
        
        <h2 class="text-uppercase" style="color: #b30000;">Avant de commencer</h2>
        
        <p style="font-size: 18px;">
            Nous vous souhaitons la bienvenue
        </p>
        
        <p style="line-height: 1.5;">
            Cette application est destinée à vous attribuer un <strong>Secret Santa</strong> de façon aléatoire. <br>
            Pour une meilleure gestion, cette application est destinée aux jeunes et membres de la jeunesse <strong>MEISTAD CERGY</strong> . <br>
            À cet effet, vous pouvez devenir membre de la jeunesse <strong>MEISTAD</strong> en contactant un responsable.
        </p>
        
        <p class="text" style="font-style: italic;">
            Cette application est une initiative des jeunes de la <strong>MEISTAD</strong> pour mieux gérer la fête de fin d'année 2024.
        </p>
        
        <p class="text" style="font-weight: bold; color: #5166b9;">
            <span><strong>CONSIGNE</strong>:</span>
            Si vous n'avez pas de compte, vous devez contacter l'administrateur pour en créer un, afin de bénéficier des fonctionnalités de l'application.
        </p>
        
        <div class="float-left" style="margin-top: 20px;">
            <a href="{{ route('auth') }}" class="btn btn-primary bg-primary">OK, Continuer</a>
        </div>
    </div>
</div>
@endsection
