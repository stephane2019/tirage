@extends('form.home')
@section('content')
@include('form.pages.alert')
<div class="wrapper col-sm-8" style="margin-top: 70px;">
    <form class="form-right" method="post" action="{{ route('user.secretUpdate', $user->id) }}">
        @csrf
        @method('PUT')
        <h2 class="text-uppercase text-center">Formulaire de secret santa</h2>
        <div class="row">
            <div class="col-sm-12 mb-3">
                <label for="nom">Binome A</label>
                <input disabled type="text" name="nom" id="nom" class="form-control input-field" required value="{{ $user->nom }} {{ $user->prenoms }}">
            </div>
        </div>
        <div class="col-sm-12 mb-3">
            <label for="binome" class="form-label">
                <i class="fas fa-user-friends"></i> Binome B <small>(Sélectionner un utilisateur)</small>
            </label>
            <select name="binome_id" id="binome" class="form-control input-field">
                <option value="">-- Sélectionner un utilisateur --</option>

                {{-- Ajouter le binôme actuel comme option sélectionnée si existant --}}
                @if ($currentBinomeUser)
                    <option value="{{ $currentBinomeUser->id }}" selected>
                        {{ $currentBinomeUser->nom }} {{ $currentBinomeUser->prenoms }}
                    </option>
                @endif

                {{-- Lister les utilisateurs non assignés --}}
                @foreach ($usersNotAssigned as $notAssigned)
                    <option value="{{ $notAssigned->id }}">
                        {{ $notAssigned->nom }} {{ $notAssigned->prenoms }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-field">
            <input type="submit" value="Enregistrer la modification" class="col-sm-12 register" name="inscription">
        </div>
    </form>
</div>
@endsection
