@extends('form.front.layouts.layout')

@section('content')
<div class="wrapper col-sm-12 col-md-6">
    <form method="POST" class="form-right" action="{{ route('register') }}">
        @csrf
        <h2 class="text-uppercase text-center">{{ __('Formulaire d\'inscription') }}</h2>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row mb-3">
            <div class="col-md-12"> 
                <label for="name">{{ __('Nom') }}</label>
                <input id="name" type="text" class="form-control @error('nom') is-invalid @enderror" name="nom" value="{{ old('nom') }}" required autocomplete="nom" autofocus>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-md-12"> 
                <label for="prenoms">{{ __('Prénoms') }}</label>
                <input id="prenoms" type="text" class="form-control @error('prenoms') is-invalid @enderror" name="prenoms" value="{{ old('prenoms') }}" required autocomplete="prenoms" autofocus>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-md-12">
                <label for="email">{{ __('Adresse email') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-md-12"> 
                <label for="password">{{ __('Mot de passe') }}</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-md-12">
                <label for="password-confirm">{{ __('Confirmation du mot de passe') }}</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-md-12">
                <label for="sexe">Sexe</label>
                <select class="form-control input-field" id="sexe" name="sexe" required>
                    <option value="">Sélectionner le sexe</option>
                    <option value="homme" {{ old('sexe') == 'homme' ? 'selected' : '' }}>Homme</option>
                    <option value="femme" {{ old('sexe') == 'femme' ? 'selected' : '' }}>Femme</option>
                </select>
            </div>
        </div>

        <div class="form-field">
            <button type="submit" class="col-sm-12 register btn btn-primary">
                {{ __('S\'inscrire') }}
            </button>
        </div>
    </form>
</div>
@endsection
