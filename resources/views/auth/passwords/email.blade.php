@extends('form.front.layouts.layout')

@section('content')
<div class="col-sm-9" style="padding: 20px 40px;
    position: relative;margin: auto;
    margin-bottom: -50px;">
    @include('form.pages.alert')
</div>
<div class="wrapper col-sm-4">
    <form class="form-right" method="POST" action="{{ route('password.email') }}">
        @csrf
        <h2 class="text-uppercase text-center">{{ __('Mot de passe oublié ?') }}</h2>
        <div class="mb-3">
            <label for="email">{{ __('Adresse Email') }}</label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-field">
            <button type="submit" class="col-sm-12 register float-right">
                {{ __('Envoyer le lien') }}
            </button>
        </div>
    </form>
</div>
@endsection
