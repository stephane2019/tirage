<?php

namespace Database\Seeders;

use App\Models\Admins;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        $admin = Admins::create([
            'name' => 'meistad',
            'email' => 'jeunesse@meistad.fr',
            'password' => 'meistad2023'
        ]);
    }
}
