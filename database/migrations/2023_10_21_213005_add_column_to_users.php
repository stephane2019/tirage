<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password');
            $table->boolean('active')->default(0);
            $table->string('sexe');
            $table->string('date_naissance');
            $table->boolean('is_visible')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password');
            $table->boolean('active')->default(0);
            $table->string('sexe');
            $table->string('date_naissance');
            $table->boolean('is_visible')->default(0);
        });
    }
}
