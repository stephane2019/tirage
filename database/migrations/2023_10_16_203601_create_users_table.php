<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('prenoms');
            $table->string('numero', 14)->unique();
            $table->boolean('is_binomeA')->default(0);
            $table->boolean('is_binomeB')->default(0);
            $table->integer('admins_id')->unsigned();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('admins_id')
                  ->references('id')
                  ->on('admins')
                  ->onDelete('restrict')
                  ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
