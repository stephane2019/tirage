<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Admins;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nom',
        'prenoms',
        'numero',
        'admins_id',
        'sexe',
        'remember_token',
        'password',
        'active',
        'is_binomeA',
        'is_binomeB',
        'date_naissance',
        'is_visible',
        'email',
        'activation_token',
        'is_participed'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return !is_null($this->admin);
    }

    public function admin()
    {
        return $this->hasOne(Admins::class, 'user_id');
    }
}
