<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Binome extends Model
{
    protected $table = 'binome';

    protected $fillable = [
        'user_id_A', 'user_id_B'
    ];

    public function utilisateurA()
    {
        return $this->belongsTo(User::class, 'user_id_A');
    }

    public function utilisateurB()
    {
        return $this->belongsTo(User::class, 'user_id_B');
    }
}
