<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Notifications\ActivationEmail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => ['required', 'string', 'max:255'],
            'prenoms' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'sexe' => ['required', 'in:homme,femme']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     */
    protected function create(array $data)
    {
        $user = User::create([
            'nom' => $data['nom'],
            'prenoms' => $data['prenoms'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'admins_id' => null,
            'sexe' => $data['sexe'],
            'activation_token' => Str::random(60)
        ]);

        if (!$user) {
            return null;
        }
    
        return $user;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        // Créez l'utilisateur
        $user = $this->create($request->all());

        // Envoi de l'e-mail d'activation
        Mail::to($user->email)->send(new ActivationEmail($user));

        // Redirection vers la page de login avec un message de succès
        return redirect()->route('auth')->with('Ok', 'Inscription effectuée avec succès ! Un e-mail d’activation vous a été envoyé.');
    }

}
