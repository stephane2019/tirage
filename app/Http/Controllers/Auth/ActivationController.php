<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ActivationController extends Controller
{
    public function activate($token)
    {
        // Recherchez l'utilisateur par le jeton d'activation
        $user = User::where('activation_token', $token)->first();

        if (!$user) {
            return redirect()->route('auth')->with('error', 'Lien d’activation invalide.');
        }

        // Activer l'utilisateur
        $user->update(['active' => 1, 'activation_token' => null]);

        return redirect()->route('auth')->with('status', 'Votre compte a été activé avec succès.');
    }
}
