<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        // Récupérer l'utilisateur par email
        $user = $this->getUserByEmail($request->only('email'));

        if ($user) {
            // Générer un token de réinitialisation
            $token = app('auth.password.broker')->createToken($user);

            // Utilisez la notification personnalisée
            $user->notify(new ResetPasswordNotification($token));
        }

        return back()->with('status', 'Un lien de réinitialisation a été envoyé à votre adresse e-mail : ' . $request->input('email'));
    }

    // Vous devez également ajouter cette méthode
    protected function getUserByEmail($request)
    {
        return \App\Models\User::where('email', $request['email'])->first();
    }
}
