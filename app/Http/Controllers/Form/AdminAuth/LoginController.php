<?php

namespace App\Http\Controllers\Form\AdminAuth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('form.admin.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::guard('admin')->attempt($credentials)) {
            return redirect()->intended('/admin/home');
        }

        return back()->with(['No' => 'Vos Idenfiants de connexion sont invalides']);
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin/auth');
    }
}
