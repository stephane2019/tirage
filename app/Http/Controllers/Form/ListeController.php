<?php

namespace App\Http\Controllers\Form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Str;
use App\Notifications\Reinitialiser;
use App\Models\Binome;

class ListeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('form.liste', compact('users'));
    }

    public function delete($id) {
        DB::table('users')->where('id', $id)->delete();
        return redirect()->route('liste')->with('Ok', 'L\'utilisateur a été supprimé avec succès.');
    }

    public function reinitialiser($id) {
        $user = User::find($id);
        $random = Str::random(8);
        $hashedPassword = bcrypt($random);
        $user->update(['password' => $hashedPassword]);
        $confirmationNotification = new Reinitialiser($user, $random);
        $confirmationNotification->send();
        return redirect()->route('liste')->with('Ok', 'Mot de passe a été renitialisé avec succès.');
    }

    public function secretSanta() {
        $binomes = Binome::with(['utilisateurA', 'utilisateurB'])->get();
        return view('form.binomes', compact('binomes'));
    }

    public function deleteBinome($id) {
        $binome = Binome::findOrFail($id);

        $userA = User::find($binome->user_id_A);
        $userA->is_binomeA = 0;
        $userA->is_binomeB = 0;
    
        $binome->delete();
        $userA->save();
    
        return redirect()->route('secret_santa')->with('success', 'Binôme supprimé avec succès.');
    }
    
}