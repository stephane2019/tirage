<?php

namespace App\Http\Controllers\Form;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Users\UserRepository;
use App\Models\User;
use Illuminate\Support\Str;
use Twilio\Rest\Client;
use App\Notifications\ConfirmationInscription;
use App\Notifications\ResultatTirage;
use Auth;
use App\Models\Binome;

class InscriptionController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form.inscription.index');
    }

    public function store(Request $request) {
        // $sid = env('TWILIO_SID');
        // $token = env('TWILIO_AUTH_TOKEN');
        // $twilioWhatsappNumber = env('TWILIO_WHATSAPP_NUMBER');
        // $twilio = new Client($sid, $token);
    
        $email = $request->input('email');

        $existingUser = User::where('email', $email)->first();
    
        if ($existingUser) {
            return redirect()->route('inscription')->with('No', 'Un utilisateur avec ce mail existe déjà.')->withInput($request->all());
        } else {
            $randomPassword = Str::random(8);
            $inputs = [
                'nom' => $request->input('nom'),
                'prenoms' => $request->input('prenoms'),
                'email' => $email,
                'numero' => null,
                'sexe' => $request->input('sexe'),
                'admins_id' => Auth::user()->id,
                'password' => $randomPassword,
                'remember_token' => $request->input('_token')
            ];
    
            $user = $this->userRepository->store($inputs);

            $confirmationNotification = new ConfirmationInscription($user);
            $confirmationNotification->send();

            $hashedPassword = bcrypt($randomPassword);
            $user->update(['password' => $hashedPassword]);
    
            // $twilio->messages->create(
            //     $numero,
            //     array(
            //         "from" => $twilioWhatsappNumber,
            //         "body" => "Votre mot de passe est ".$randomPassword
            //     )
            // );
    
            return redirect()->route('inscription')->with('Ok', 'Inscription effectuée avec succès');
        }
    }
    
    public function edit($id) {
        $user = User::find($id);
        return view('form.inscription.edit', ['user' => $user]);
    }

    public function update(Request $request, $id) {
        // Validez les données entrées par l'utilisateur
        $request->validate([
            'nom' => 'required',
            'prenoms' => 'required',
            'email' => 'required',
            'sexe' => 'required'
        ]);
    
        $user = User::find($id);
    
        if (!$user) {
            // Gérer le cas où l'utilisateur n'est pas trouvé (par exemple, rediriger vers une page d'erreur)
        }

        $user->nom = $request->input('nom');
        $user->prenoms = $request->input('prenoms');
        $user->email = $request->input('email');
        $user->sexe = $request->input('sexe');
        $user->is_participed = $request->has('is_participed') ? 1 : 0;
        // $user->is_binomeA = $request->has('is_binomeA') ? 1 : 0;
        // $user->is_binomeB = $request->has('is_binomeB') ? 1 : 0;
        $user->save();
    
        return redirect()->route('user.edit', ['id' => $user->id])->with('Ok', 'Les informations de l\'utilisateur ont été mises à jour avec succès.');
    }

    public function secretEdit($id)
    {
        $user = User::find($id);

        // Récupère le binôme existant pour cet utilisateur
        $binome = Binome::where('user_id_A', $id)->first();
        $currentBinomeUser = null;

        if ($binome) {
            $currentBinomeUser = User::find($binome->user_id_B);
        }

        // Récupère les utilisateurs qui n'ont pas encore de binôme
        $usersNotAssigned = User::where('is_participed', 1)
            ->where('is_binomeB', 0)
            ->where('id', '!=', $id)
            ->get();

        return view('form.edit-binome', [
            'user' => $user,
            'currentBinomeUser' => $currentBinomeUser,
            'usersNotAssigned' => $usersNotAssigned,
        ]);
    }

    public function secretUpdate(Request $request, $id) {
        // Valider les données envoyées par le formulaire
        $validated = $request->validate([
            'binome_id' => 'nullable|exists:users,id',
        ]);
    
        // Trouve l'utilisateur auquel nous voulons ajouter un binôme
        $user = User::find($id);
    
        // Trouver l'ancien binôme (utilisateur B) associé à l'utilisateur A
        $binome = Binome::where('user_id_A', $id)->first();
    
        if ($binome) {
            // Si un binôme existe déjà, mettez à jour l'ID du binôme B
            $oldBinomeUser = $binome->utilisateurB; // Ancien utilisateur B
            
            // Mettre à jour l'ancien utilisateur B (is_binomeB = 0)
            if ($oldBinomeUser) {
                $oldBinomeUser->is_binomeB = 0;
                $oldBinomeUser->save();
            }
        }
    
        // Mise à jour la table Binome avec le nouveau binôme
        $binome->user_id_B = $request->binome_id;
        $binome->save();
    
        // Mise à jour l'utilisateur B sélectionné (is_binomeB = 1)
        $newBinomeUser = User::find($request->binome_id);
        if ($newBinomeUser) {
            $newBinomeUser->is_binomeB = 1;
            $newBinomeUser->save();
        }

        $binomeUtilisateurConnecte = Binome::where('user_id_A', $id)->first();

        $notificationTirage = new ResultatTirage($binomeUtilisateurConnecte);
        $notificationTirage->send();
    
        // Redirection avec un message de succès
        return redirect()->route('user.secret', ['id' => $user->id])
            ->with('Ok', 'Binôme mis à jour avec succès');
    }      
    
   
}