<?php

namespace App\Http\Controllers\Form\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Binome;
use App\Notifications\ResultatTirage;
use Auth;

class TirageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form.users.index');
    }

    public function create()
    {
        if (Auth::user()->is_binomeA && Auth::user()->is_participed) {
            return redirect()->route('resultat');
        }
        return view('form.front.tirage');
    }

    /**
     * Cette methode permet d'attribuer des binôme de façon aleatoire.
     */
    public function attribuerBinomeAleatoire()
    {
        // Vérifie si l'utilisateur a déjà un binôme
        if (Auth::user()->is_binomeA && Auth::user()->is_participed) {
            return redirect()->route('resultat')->with('No', 'Vous avez déjà un binôme.');
        }

        // Sélectionne l'utilisateur actif et non encore attribué à un binôme
        $utilisateur = User::where('active', 1)
            // ->where('is_binomeA', 0)
            ->where('is_binomeB', 0)
            ->where('is_participed', 1)
            ->where('id', '!=', Auth::user()->id)
            ->inRandomOrder()
            ->first();

        // S'assurez qu'il y a au moins un utilisateur pour attribuer un binôme
        if (!$utilisateur) {
            return redirect()->route('resultat')->with('No', 'Pas suffisamment d\'utilisateurs pour attribuer des binômes.');
        }

        // Créez un enregistrement de binôme pour l'utilisateur connecté
        $binome = new Binome();
        $binome->user_id_A = Auth::user()->id;
        $binome->user_id_B = $utilisateur->id;
        $binome->save();

        // Mettre à jour les utilisateurs pour indiquer qu'ils ont un binôme
        Auth::user()->update(['is_binomeA' => 1]);
        $utilisateur->update(['is_binomeB' => 1]);

        // Récupérez le binôme de l'utilisateur connecté
        $binomeUtilisateurConnecte = Binome::where('user_id_A', Auth::user()->id)
        ->orWhere('user_id_B', Auth::user()->id)
        ->first();

        $notificationTirage = new ResultatTirage($binomeUtilisateurConnecte);
        $notificationTirage->send();

        return view('form.front.resultat', [
            'binomeUtilisateurConnecte' => $binomeUtilisateurConnecte
        ])->with('Ok', 'Binôme attribué avec succès.');
    }

    /**
     * Represente le resultat de la recherche de binome pour un user connecté.
     */
    public function resultat()
    {
        // Obtenir le binôme de l'utilisateur connecté :
        $binomeUtilisateurConnecte = Binome::where('user_id_A', Auth::user()->id)
            ->first();

        if (empty($binomeUtilisateurConnecte)) {
            return view('form.front.resultat')->with('No', 'Vous n\'avez pas de binôme pour le moment.');
        }

        return view('form.front.resultat', [
            'binomeUtilisateurConnecte' => $binomeUtilisateurConnecte
        ]);
    }

    /**
     * Les détails de l'utilisateur connecté.
    */
    public function profil()
    {
        // Obtenir le user connecté :
        $user = User::where('id', Auth::user()->id)->first();

        return view('form.front.profil', [
            'user' => $user
        ]);
    }

    public function update(Request $request, $id) {
        // Validez les données entrées par l'utilisateur
        $request->validate([
            'nom' => 'required',
            'prenoms' => 'required',
            'email' => 'required',
            'sexe' => 'required'
        ]);
    
        $user = User::find($id);

        $user->nom = $request->input('nom');
        $user->prenoms = $request->input('prenoms');
        $user->email = $request->input('email');
        $user->sexe = $request->input('sexe');
        $user->date_naissance = $request->input('date_naissance');
        $user->is_visible = $request->has('visible_age') ? 1 : 0;
        $user->save();
    
        return redirect()->route('profil', ['id' => $user->id])->with('Ok', 'Mes informations ont été mises à jour avec succès.');
    }

    public function logout(Request $request)
    {
        Auth::guard('web')->logout();
        return redirect('/auth');
    }

    public function home() {
        return view("form.front.home");
    }
}