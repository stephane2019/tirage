<?php

namespace App\Http\Controllers\Form\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AuthentificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form.auth.index');
    }

    public function welcome()
    {
        return view('form.auth.welcome');
    }

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if ($user->active == 1) {
                return redirect()->intended('/home');
            } else {
                return view('form.auth.verification');
            }
        }

        return back()->with(['No' => 'Vos Idenfiants de connexion sont invalides']);
    }

    public function verification(Request $request) {
        $user = Auth::user();
        if ($user && $user->active !== 1) {
            $user->update(['active' => 1, 'password' =>bcrypt($request->input('password'))]);

            return redirect()->route('home');
        }
    }
}