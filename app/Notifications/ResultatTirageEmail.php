<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Binome;

class ResultatTirageEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $binomeUtilisateurConnecte;

    public function __construct(Binome $binomeUtilisateurConnecte)
    {
        $this->binomeUtilisateurConnecte = $binomeUtilisateurConnecte;
    }

    public function build()
    {
        return $this->view('emails.resultat')
            ->with([
                'binomeUtilisateurConnecte' => $this->binomeUtilisateurConnecte
            ])
            ->subject('Jeunesse Meistad: Votre résultat');
    }
}