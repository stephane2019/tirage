<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\User;

class ReinitialiserEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $password;

    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    public function build()
    {
        return $this->view('emails.reinitialiser')
            ->with([
                'user' => $this->user,
                'password' => $this->password,
                'confirmationLink' => url('/auth')
            ])
            ->subject('Jeunesse Meistad: Reinitialisation du mot de passe');
    }
}