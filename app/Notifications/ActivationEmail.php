<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->subject('Confirmez votre inscription')
                    ->view('emails.activation')
                    ->with([
                        'user' => $this->user,
                        'activationLink' => route('user.activate', ['token' => $this->user->activation_token]),
                    ]);
    }
}
