<?php

namespace App\Notifications;

use App\Notifications\ConfirmationInscriptionMail;
use Illuminate\Support\Facades\Mail;
use App\Models\User;

class ConfirmationInscription
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function send()
    {
        Mail::to($this->user->email)->send(new ConfirmationInscriptionMail($this->user));
    }
}
