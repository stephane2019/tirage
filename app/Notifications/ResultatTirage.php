<?php

namespace App\Notifications;

use App\Notifications\ResultatTirageEmail;
use Illuminate\Support\Facades\Mail;
use App\Models\Binome;

class ResultatTirage
{
    protected $binomeUtilisateurConnecte;

    public function __construct(Binome $binomeUtilisateurConnecte)
    {
        $this->binomeUtilisateurConnecte = $binomeUtilisateurConnecte;
    }

    public function send()
    {
        Mail::to($this->binomeUtilisateurConnecte->utilisateurA->email)->send(new ResultatTirageEmail($this->binomeUtilisateurConnecte));
    }
}