<?php

namespace App\Notifications;

use App\Notifications\ReinitialiserEmail;
use Illuminate\Support\Facades\Mail;
use App\Models\User;

class Reinitialiser
{
    protected $user;
    protected $password;

    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    public function send()
    {
        Mail::to($this->user->email)->send(new ReinitialiserEmail($this->user, $this->password));
    }
}