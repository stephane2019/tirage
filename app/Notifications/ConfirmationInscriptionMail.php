<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\User;

class ConfirmationInscriptionMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->view('emails.confirmation')
            ->with([
                'user' => $this->user,
                'confirmationLink' => url('/auth'),
            ])
            ->subject('Jeunesse Meistad: Confirmation inscription');
    }
}