<?php
namespace App\Repositories\Users;

use App\Repositories\ResourceRepository;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository extends ResourceRepository
{
    public function __construct(User $user)
    {
        $this->model=$user;
    }

    public function find_user($id)
    {
    	return $this->model->where('id',$id)->first();
    }
    
    /**
     * @return Collection|static[]
     */
    public function all_desc(){
        return $this->model->orderBy('id','desc')->get();
    }
}