<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Form\InscriptionController; 
use App\Http\Controllers\Form\ListeController; 
use App\Http\Controllers\Form\Front\AuthentificationController; 
use App\Http\Controllers\Form\Front\TirageController; 
use App\Http\Controllers\Form\AdminAuth\LoginController; 
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Form\Front\ForgotPasswordController; 
use App\Http\Controllers\Auth\ActivationController;
use App\Http\Controllers\Auth\RegisterController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/activate/{token}', [ActivationController::class, 'activate'])->name('user.activate');
Route::post('register', [RegisterController::class, 'register']);

Route::namespace('Form')->group(function () {

    Route::namespace('AdminAuth')->group(function () {
        Route::get('/admin/auth', [LoginController::class, 'showLoginForm'])->name('login');
        Route::post('/admin/login', [LoginController::class, 'login'])->name('admin.login');
        
        Route::group(['middleware' => 'auth:admin'], function () {
            Route::post('/admin/logout', [LoginController::class, 'logout'])->name('admin.logout');
        });
    });

    Route::namespace('Front')->group(function () {

        Route::get('/welcome', [AuthentificationController::class, 'welcome'])->name('welcome');

        Route::get('/auth', [AuthentificationController::class, 'index'])->name('auth');
        Route::post('/user/login', [AuthentificationController::class, 'login'])->name('user.login');

        // Route pour la réinitialisation du mot de passe
        Route::get('/mot-de-passe-oublie', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
        Route::post('/mot-de-passe-oublie', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
        
        Route::get('/reinitialiser-mot-de-passe/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
        Route::post('/reinitialiser-mot-de-passe', [ResetPasswordController::class, 'reset'])->name('password.update');
        
        Route::group(['middleware' => 'auth'], function () {
            Route::get('/tirage', [TirageController::class, 'create'])->name('tirage');
            Route::put('/user/verification', [AuthentificationController::class, 'verification'])->name('verification');
            Route::get('/tirage/attribuer', [TirageController::class, 'attribuerBinomeAleatoire'])->name('tirage.attribuer');
            Route::get('/resultat', [TirageController::class, 'resultat'])->name('resultat');
            Route::get('/mon-profil', [TirageController::class, 'profil'])->name('profil');
            Route::put('/user/update/{id}', [TirageController::class, 'update'])->name('profil.update');
            Route::post('/user/logout', [TirageController::class, 'logout'])->name('user.logout');
            Route::get('/home', [TirageController::class, 'home'])->name('home');
        });
    });

    Route::group(['middleware' => 'auth:admin'], function () {
        // Route inscription
        Route::get('/admin/inscription', [InscriptionController::class, 'index'])->name('inscription');
        Route::post('/admin/inscription', [InscriptionController::class, 'store'])->name('user.store');
        Route::get('/admin/user/edit/{id}', [InscriptionController::class, 'edit'])->name('user.edit');
        Route::put('/admin/user/update/{id}', [InscriptionController::class, 'update'])->name('user.update');
        
        Route::get('/admin/user/delete/{id}', [ListeController::class, 'delete'])->name('user.delete');
        Route::get('/admin/user/reinitialiser/{id}', [ListeController::class, 'reinitialiser'])->name('user.reinitialiser');
        Route::get('/admin/liste', [ListeController::class, 'index'])->name('liste');
        Route::get('/admin/secret-santa', [ListeController::class, 'secretSanta'])->name('secret_santa');

        Route::delete('/admin/secret-santa/{id}', [ListeController::class, 'deleteBinome'])->name('binomes.delete');

        Route::get('/admin/home', [HomeController::class, 'index'])->name('admin.home');

        Route::get('/admin/user/edit/secret-santa/{id}', [InscriptionController::class, 'secretEdit'])->name('user.secret');

        Route::put('/admin/user/update/secret-santa/{id}', [InscriptionController::class, 'secretUpdate'])->name('user.secretUpdate');
    });
});

Route::redirect('/', '/welcome');

Auth::routes();
