document.addEventListener('DOMContentLoaded', function () {
    const dateNaissanceInput = document.getElementById('date_naissance');
    const visibleAgeCheckbox = document.getElementById('visibleAgeCheckbox');
    const visibleCheckbox = document.getElementById('visibleCheckbox');
    const ageInput = document.getElementById('age');
    let age;  // Déclarer la variable age ici

    dateNaissanceInput.addEventListener('change', function () {
        const selectedDate = new Date(dateNaissanceInput.value);
        const today = new Date();

        if (selectedDate > today) {
            dateNaissanceInput.value = '';
            return;
        }

        visibleAgeCheckbox.style.display = 'block';
        visibleCheckbox.style.display = 'block';
        age = calculateAge(dateNaissanceInput.value);  // Mettre à jour la variable age

        if (age < 18) {
            document.getElementById('ageError').innerText = "L'âge minimum éligible est 18 ans.";
            dateNaissanceInput.value = '';
            visibleAgeCheckbox.style.display = 'none';
            visibleCheckbox.style.display = 'none';
        } else {
            ageInput.value = age + ' ans';
            document.getElementById('ageError').innerText = "";
        }
    });

    function calculateAge(dateOfBirth) {
        const today = new Date();
        const birthDate = new Date(dateOfBirth);
        let age = today.getFullYear() - birthDate.getFullYear();

        if (today.getMonth() < birthDate.getMonth() || (today.getMonth() === birthDate.getMonth() && today.getDate() < birthDate.getDate())) {
            age--;
        }

        return age;  // Retourner la valeur de l'âge
    }
});