$(document).ready(function() {
    $('#togglePassword').click(function() {
        // Toggle the type attribute
        const type = $('#pwd').attr('type') === 'password' ? 'text' : 'password';
        $('#pwd').attr('type', type);
        
        // Toggle the eye icon
        $(this).find('i').toggleClass('fa-eye fa-eye-slash');
    });
});