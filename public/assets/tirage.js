document.getElementById('tirage-btn').addEventListener('click', function() {
    document.getElementById('loading-msg').style.display = 'block';
    this.style.display = 'none';
    setTimeout(function() {
        window.location.href = "{{ route('tirage.attribuer') }}";
    }, 30000);
});